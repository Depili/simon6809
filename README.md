# Simon6809

This is the first educational 6809 board I built back in 2007.  

- Hardware files require [ExpressPCB](http://www.expresspcb.com)
- Software files require [assembler by Lennart-Benschop](https://github.com/6809/sbc09)

Enjoy.

![SimonSide](/docs/Simon6809_side.jpg)

![SimonBlockDiagram](/docs/simon6809_blockdiagram.png)

![SimonTop](/docs/Simon6809_top.jpg)
